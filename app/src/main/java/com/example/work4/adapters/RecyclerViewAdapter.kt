package com.example.work4.adapters

import android.graphics.Color
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.work4.R
import com.example.work4.callbacks.OnClickListener
import kotlinx.android.synthetic.main.layout_recycler_view.view.*

class RecyclerViewAdapter(val onClickListener: OnClickListener): RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
    var arr = arrayListOf<Int>()
    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        fun onBind(){
            d("infoinfo","recycler $arr")
            when {
                arr[adapterPosition] == 1 -> {
                   // itemView.imageButton.setBackgroundResource(R.mipmap.ic_x)
                    itemView.imageButton.setImageResource(R.mipmap.ic_x)
                }
                arr[adapterPosition] == 2 -> {
                    //itemView.imageButton.setBackgroundResource(R.mipmap.ic_o)
                    itemView.imageButton.setImageResource(R.mipmap.ic_o)

                }
                else -> {
                    itemView.imageButton.setImageResource(0)
                    itemView.imageButton.setBackgroundColor(Color.parseColor("#ffffff"))
                }
            }
            itemView.imageButton.setOnClickListener { onClickListener.onClick(adapterPosition) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_recycler_view,parent,false))
    }

    override fun getItemCount() = arr.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }
    fun setData(arr:ArrayList<Int>){
        this.arr = arr
        notifyDataSetChanged()
    }
}