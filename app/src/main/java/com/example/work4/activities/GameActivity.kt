package com.example.work4.activities

import android.app.SharedElementCallback
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.work4.R
import com.example.work4.activities.utils.IntentKeys
import com.example.work4.adapters.RecyclerViewAdapter
import com.example.work4.callbacks.OnClickListener
import kotlinx.android.synthetic.main.activity_main.*

class GameActivity : AppCompatActivity() {
    lateinit var recyclerViewAdapter: RecyclerViewAdapter
    lateinit var layout: RecyclerView.LayoutManager
    lateinit var arr: ArrayList<Int>
    var size: Int = 0
    var x = 1
    var playerOne = 0
    var playerTwo = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        size = intent.extras!!.getInt(IntentKeys.GAME_TYPE_KEY,3)
        arr = arrayListOf()
        recyclerViewAdapter =
            RecyclerViewAdapter(object :
                OnClickListener {
                override fun onClick(position: Int) {
                    if (arr[position] != 0) {
                        return
                    }
                    x++
                    if (x % 2 == 0) {
                        arr[position] = 1
                    } else {
                        arr[position] = 2
                    }
                    recyclerViewAdapter.setData(arr)
                    gameLogic(position)
                }

            })
        newGame()
        layout = GridLayoutManager(this, size)
        recyclerView.apply {
            layoutManager = layout
            adapter = recyclerViewAdapter
        }
    }

    private fun newGame() {
        if (arr.isNotEmpty()) {
            arr.clear()
            recyclerViewAdapter.setData(arr)
        }
        for (i in 0 until size * size) {
            arr.add(0)
        }
        recyclerViewAdapter.setData(arr)
    }

    fun gameLogic(position: Int) {
        for (i in 0 until size) {
            if (lineVertical(i)) {
                win(position)
                newGame()
               return
            }
        }
        for (i in 0 until size) {
            if (lineHorizonatal(i * size)) {
                win(position)
                newGame()
                return
            }
        }
        if (diagonalLine()) {
            win(position)
            newGame()
            return
        }
        if (diagonalLine2()) {
            win(position)
            newGame()
            return
        }
        if (nichya()) {
            Toast.makeText(this, "nichya", Toast.LENGTH_SHORT).show()
            newGame()
            return
        }

    }

    private fun lineVertical(position: Int): Boolean {
        var bool = true
        for (i in 1 until size) {
            if (arr[position] == 0 || arr[size * i + position] == 0) {
                return false
            }
            if (arr[position] != arr[size * i + position]) {
                bool = false
                break
            }

        }
        return bool
    }

    private fun lineHorizonatal(position: Int): Boolean {
        val bool = true
        for (i in 1 until size) {
            if (arr[position] == 0 || arr[position + i] == 0) {
                return false
            }
            if (arr[position] != arr[position + i]) {
                return false
            }
        }
        return bool
    }

    private fun diagonalLine(): Boolean {
        var bool = true
        for (i in 1 until size) {
            if (arr[0] == 0 || arr[(size * i) + i] == 0) {
                return false
            }
            if (arr[0] != arr[(size * i) + i]) {
                bool = false
            }
        }
        return bool
    }

    private fun diagonalLine2(): Boolean {
        var bool = true
        for (i in 2..size) {
            if (arr[size - 1] == 0 || arr[(size - 1) * i] == 0) {
                return false
            }
            if (arr[size - 1] != arr[(size - 1) * i]) {
                bool = false
            }
        }
        return bool
    }
    private fun win(position: Int) {
        val mes: String
        if (arr[position] == 1) {
           mes = "win 1 player"
            playerOne++
            player1TextView.text = "player1 score: $playerOne"
        } else {
           mes = "win 2 player"
            playerTwo++
            player2TextView.text = "player2 score: $playerTwo"
        }
        x=1
        Toast.makeText(this, mes, Toast.LENGTH_SHORT).show()
    }

    private fun nichya(): Boolean {
        var bool = true
        for (i in 0 until arr.size) {
            if (arr[i] == 0) {
                bool = false
                break
            }
        }
        return bool
    }
}
