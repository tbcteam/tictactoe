package com.example.work4.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import com.example.work4.R
import com.example.work4.activities.utils.IntentKeys
import com.example.work4.callbacks.OnClickListener
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        init()
    }

    private fun init(){
        x3Button.setOnClickListener(this)
        x4Button.setOnClickListener(this)
        x5Button.setOnClickListener(this)
    }
    override fun onClick(v: View?) {
        var gameType = 0
        when(v!!.id){
            R.id.x3Button->{
                gameType = 3
            }
            R.id.x4Button->{
                gameType = 4
            }
            R.id.x5Button->{
                gameType = 5
            }
        }
        d("infoinfo","clicked")
        val intent = Intent(baseContext,GameActivity::class.java)
        intent.putExtra(IntentKeys.GAME_TYPE_KEY,gameType)
        intent.addFlags( Intent.FLAG_ACTIVITY_NO_ANIMATION)
        startActivity(intent)
    }
}
