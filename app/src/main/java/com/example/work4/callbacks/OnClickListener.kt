package com.example.work4.callbacks

interface OnClickListener {
    fun onClick(position:Int)
}